package edu.ubb.cs.idde.beim1596_dto.model;

import java.io.Serializable;

public class GenreDto implements Serializable{

	private Long id;
	private String name;
	private String features;
	private String representative;
	
	public GenreDto() {
		
	}

	public GenreDto(Long id, String name, String features, String representative) {
		super();
		this.id = id;
		this.name = name;
		this.features = features;
		this.representative = representative;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFeatures() {
		return features;
	}

	public void setFeatures(String features) {
		this.features = features;
	}

	public String getRepresentative() {
		return representative;
	}

	public void setRepresentative(String representative) {
		this.representative = representative;
	}
	
}
