package edu.ubb.cs.idde.beim1596_dto.model;

import java.io.Serializable;

public class SongDto implements Serializable {
	
	private Long id;
	private String artist;
	private String title;
	private String album;
	private String citation;
	private Integer year;
	
	public SongDto() {
	}

	public SongDto(Long id, String artist, String title, String album, String citation, Integer year) {
		super();
		this.id = id;
		this.artist = artist;
		this.title = title;
		this.album = album;
		this.citation = citation;
		this.year = year;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAlbum() {
		return album;
	}

	public void setAlbum(String album) {
		this.album = album;
	}

	public String getCitation() {
		return citation;
	}

	public void setCitation(String citation) {
		this.citation = citation;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}
	
	
}
