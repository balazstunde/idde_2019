package edu.ubb.cs.idde.beim1596_dto.model;

import java.io.Serializable;
import java.util.Date;

public class ArtistDto implements Serializable{
	
	private Long id;
	private String name;
	private Date birthday;
	private String email;
	
	public ArtistDto() {
		
	}

	public ArtistDto(Long id, String name, Date birthday, String email) {
		super();
		this.id = id;
		this.name = name;
		this.birthday = birthday;
		this.email = email;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
}
