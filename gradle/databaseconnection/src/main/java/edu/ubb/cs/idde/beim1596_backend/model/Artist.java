package edu.ubb.cs.idde.beim1596_backend.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "artists")
public class Artist extends BaseEntity{

	@Column(name = "artist_name")
	private String name;
	@Column(name = "birthday")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date birthday;
	@Column(name = "email")
	private String email;
	
	public Artist() {
		
	}
	
	public Artist(Long id, String name, Date birthday, String email) {
		super();
		this.id = id;
		this.name = name;
		this.birthday = birthday;
		this.email = email;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
}
