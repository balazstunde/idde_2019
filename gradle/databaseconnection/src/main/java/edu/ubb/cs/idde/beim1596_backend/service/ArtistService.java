package edu.ubb.cs.idde.beim1596_backend.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.ubb.cs.idde.beim1596_backend.model.Artist;
import edu.ubb.cs.idde.beim1596_backend.repository.ArtistRepository;

@Service
public class ArtistService {

	@Autowired
	private ArtistRepository artistRepository;
	
	public List<Artist> findAll(){
		List<Artist> artists = null;
		try{
			artists = artistRepository.findAll();
		}catch(Exception e) {
			throw new ServiceException("Exception in service layer.", e);
		}
		return artists;
	}
	
	public Artist create(Artist artist) {
		try {
			return artistRepository.save(artist);
		}catch(Exception e) {
			throw new ServiceException("Exception in service layer.", e);
		}
	}
	
	public void update(Artist artist) {
		try {
			artistRepository.save(artist);
		}catch(Exception e) {
			throw new ServiceException("Exception in service layer.", e);
		}
	}
	
	public void delete(Artist artist) {
		try {
			artistRepository.delete(artist);
		}catch(Exception e) {
			throw new ServiceException("Exception in service layer.", e);
		}
	}
	
	public Artist getById(Long id) {
		return artistRepository.findById(id).get();
	}
}
