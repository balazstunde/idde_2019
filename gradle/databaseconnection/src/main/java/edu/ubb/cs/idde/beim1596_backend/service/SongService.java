package edu.ubb.cs.idde.beim1596_backend.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.ubb.cs.idde.beim1596_backend.model.Song;
import edu.ubb.cs.idde.beim1596_backend.repository.SongRepository;

@Service
public class SongService {
	
	@Autowired
	private SongRepository songRepository;
	
	public List<Song> findAll(){
		List<Song> songs = null;
		try{
			songs = songRepository.findAll();
		}catch(Exception e) {
			throw new ServiceException("Exception in service layer.", e);
		}
		return songs;
	}
	
	public Song create(Song song) {
		try {
			return songRepository.save(song);
		}catch(Exception e) {
			throw new ServiceException("Exception in service layer.", e);
		}
	}
	
	public void update(Song song) {
		try {
			songRepository.save(song);
		}catch(Exception e) {
			throw new ServiceException("Exception in service layer.", e);
		}
	}
	
	public void delete(Song song) {
		try {
			songRepository.delete(song);
		}catch(Exception e) {
			throw new ServiceException("Exception in service layer.", e);
		}
	}
	
	public Song getById(Long id) {
		return songRepository.findById(id).get();
	}
}
