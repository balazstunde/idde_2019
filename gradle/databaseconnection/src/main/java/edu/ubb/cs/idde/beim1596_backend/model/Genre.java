package edu.ubb.cs.idde.beim1596_backend.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "genres")
public class Genre extends BaseEntity{

	@Column(name = "genres_name")
	private String name;
	@Column(name = "features")
	private String features;
	@Column(name = "representative")
	private String representative;
	
	public Genre() {
		
	}
	
	public Genre(Long id, String name, String features, String representative) {
		super();
		this.id = id;
		this.name = name;
		this.features = features;
		this.representative = representative;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFeatures() {
		return features;
	}

	public void setFeatures(String features) {
		this.features = features;
	}

	public String getRepresentative() {
		return representative;
	}

	public void setRepresentative(String representative) {
		this.representative = representative;
	}
	
}
