package edu.ubb.cs.idde.beim1596_backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import edu.ubb.cs.idde.beim1596_backend.model.Artist;

@Repository
public interface ArtistRepository extends JpaRepository<Artist, Long>{

}
