package edu.ubb.cs.idde.beim1596_backend.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.ubb.cs.idde.beim1596_backend.model.Genre;
import edu.ubb.cs.idde.beim1596_backend.repository.GenreRepository;

@Service
public class GenreService {
	
	@Autowired
	private GenreRepository genreRepository;
	
	public List<Genre> findAll(){
		List<Genre> genres = null;
		try{
			genres = genreRepository.findAll();
		}catch(Exception e) {
			throw new ServiceException("Exception in service layer.", e);
		}
		return genres;
	}
	
	public Genre getById(Long id) {
		return genreRepository.findById(id).get();
	}
	
	public Genre create(Genre genre) {
		try {
			return genreRepository.save(genre);
		}catch(Exception e) {
			throw new ServiceException("Exception in service layer.", e);
		}
	}
	
	public void update(Genre genre) {
		try {
			genreRepository.save(genre);
		}catch(Exception e) {
			throw new ServiceException("Exception in service layer.", e);
		}
	}
	
	public void delete(Genre genre) {
		try {
			genreRepository.delete(genre);
		}catch(Exception e) {
			throw new ServiceException("Exception in service layer.", e);
		}
	}
}
