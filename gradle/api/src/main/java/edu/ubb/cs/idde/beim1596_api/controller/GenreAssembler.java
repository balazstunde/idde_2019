package edu.ubb.cs.idde.beim1596_api.controller;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import edu.ubb.cs.idde.beim1596_backend.model.Genre;
import edu.ubb.cs.idde.beim1596_dto.model.GenreDto;

@Component
public class GenreAssembler extends AbstractAssembler<Genre, GenreDto> {

	@Autowired
    private ModelMapper modelMapper;
	
	@Override
	public GenreDto modelToDto(Genre model) {
		GenreDto genreDto = modelMapper.map(model, GenreDto.class);
	    return genreDto;
	}
	
	@Override
	public Genre dtoToModel(GenreDto dto) {
		Genre genre = modelMapper.map(dto, Genre.class);
	    return genre;
	}
}
