package edu.ubb.cs.idde.beim1596_api;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

public class ServletInitializer extends SpringBootServletInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        System.out.println("edu.ubb.cs.idde.api.ServletInitializer called.");
        return application.sources(Application.class);
    }
}
