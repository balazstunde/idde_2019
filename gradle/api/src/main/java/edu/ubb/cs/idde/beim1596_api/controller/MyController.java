package edu.ubb.cs.idde.beim1596_api.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.ubb.cs.idde.beim1596_backend.model.Artist;
import edu.ubb.cs.idde.beim1596_backend.model.Genre;
import edu.ubb.cs.idde.beim1596_backend.model.Song;
import edu.ubb.cs.idde.beim1596_backend.service.ArtistService;
import edu.ubb.cs.idde.beim1596_backend.service.GenreService;
import edu.ubb.cs.idde.beim1596_backend.service.SongService;
import edu.ubb.cs.idde.beim1596_dto.model.ArtistDto;
import edu.ubb.cs.idde.beim1596_dto.model.GenreDto;
import edu.ubb.cs.idde.beim1596_dto.model.SongDto;

@Controller
public class MyController {
	
	@RequestMapping(method = RequestMethod.GET, value = "/")
	@ResponseBody
	public String hello() {
		return "welcome";
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/home")
	public String hello1() {
		return "welcome";
	}
	
	@Autowired
	private SongService songService;
	
	@Autowired
	private ArtistService artistService;
	
	@Autowired
	private GenreService genreService;
	
	@Autowired
	private GenreAssembler genreAssembler;
	
	@Autowired
	private SongAssembler songAssembler;
	
	@Autowired
	private ArtistAssembler artistAssembler;

	@RequestMapping(method = RequestMethod.GET, value = "/songs")
	@ResponseBody
	 public List<SongDto> getSongs() {
		List<Song> songs = null;
		try {
			songs = songService.findAll();
		}catch(DataAccessException ex) {
			System.out.println(ex.toString());
		}
		List<SongDto> songDtos = new ArrayList<SongDto>();
		for (Song song : songs) {
			SongDto songDto = songAssembler.modelToDto(song);
			songDtos.add(songDto);
		}
		return songDtos;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/artists")
	@ResponseBody
	 public List<ArtistDto> getArtists() {
		List<Artist> artists = null;
		try {
			artists = artistService.findAll();
		}catch(DataAccessException ex) {
			System.out.println(ex.toString());
		}
		List<ArtistDto> artistDtos = new ArrayList<ArtistDto>();
		for (Artist artist : artists) {
			ArtistDto artistDto = artistAssembler.modelToDto(artist);
			artistDtos.add(artistDto);
		}
		return artistDtos;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/genres")
	@ResponseBody
	 public List<GenreDto> getGenres() {
		List<Genre> genres = null;
		try {
			genres = genreService.findAll();
		}catch(DataAccessException ex) {
			System.out.println(ex.toString());
		}
		List<GenreDto> genreDtos = new ArrayList<GenreDto>();
		for (Genre genre : genres) {
			GenreDto genreDto = genreAssembler.modelToDto(genre);
			genreDtos.add(genreDto);
		}
		return genreDtos;
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value = "/genres/{id}")
	@ResponseBody
	 public void deleteGenre(@PathVariable Long id) {
		Genre genre = genreService.getById(id);
		genreService.delete(genre);
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value = "/artists/{id}")
	@ResponseBody
	 public void deleteArtist(@PathVariable Long id) {
		Artist artist = artistService.getById(id);
		artistService.delete(artist);
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value = "/songs/{id}")
	@ResponseBody
	 public void deleteSong(@PathVariable Long id) {
		Song song = songService.getById(id);
		songService.delete(song);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/genres")
	@ResponseBody
	 public Genre saveGenre(@RequestBody Genre genre) {
		return genreService.create(genre);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/artists")
	@ResponseBody
	 public Artist saveArtist(@RequestBody Artist artist) {
		return artistService.create(artist);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/songs")
	@ResponseBody
	 public Song saveSong(@RequestBody Song song) {
		return songService.create(song);
	}
}
