package edu.ubb.cs.idde.beim1596_api.controller;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import edu.ubb.cs.idde.beim1596_backend.model.Song;
import edu.ubb.cs.idde.beim1596_dto.model.SongDto;

@Component
public class SongAssembler extends AbstractAssembler<Song, SongDto> {

	@Autowired
    private ModelMapper modelMapper;
	
	@Override
	public SongDto modelToDto(Song model) {
		SongDto songDto = modelMapper.map(model, SongDto.class);
	    return songDto;
	}
	
	@Override
	public Song dtoToModel(SongDto dto) {
		Song song = modelMapper.map(dto, Song.class);
	    return song;
		
	}
}