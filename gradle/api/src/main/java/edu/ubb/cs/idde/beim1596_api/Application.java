package edu.ubb.cs.idde.beim1596_api;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = {"edu.ubb.cs.idde.beim1596_api", "edu.ubb.cs.idde.beim1596_backend"})
@EntityScan(basePackages = {"edu.ubb.cs.idde.beim1596_backend.model"})
@EnableJpaRepositories(basePackages = {"edu.ubb.cs.idde.beim1596_backend.repository"})
public class Application {
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
	/*@Bean 
	ServletWebServerFactory servletWebServerFactory(){
		return new TomcatServletWebServerFactory();
	}*/
	
	@Bean
	public ModelMapper modelMapper() {
	    return new ModelMapper();
	}
}
