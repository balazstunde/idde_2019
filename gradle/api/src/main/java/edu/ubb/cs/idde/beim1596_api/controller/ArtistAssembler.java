package edu.ubb.cs.idde.beim1596_api.controller;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import edu.ubb.cs.idde.beim1596_backend.model.Artist;
import edu.ubb.cs.idde.beim1596_dto.model.ArtistDto;

@Component
public class ArtistAssembler extends AbstractAssembler<Artist, ArtistDto> {

	@Autowired
    private ModelMapper modelMapper;
	
	@Override
	public ArtistDto modelToDto(Artist model) {
		ArtistDto artistDto = modelMapper.map(model, ArtistDto.class);
	    return artistDto;
	}
	
	@Override
	public Artist dtoToModel(ArtistDto dto) {
		Artist artist = modelMapper.map(dto, Artist.class);
	    return artist;
	}
}
