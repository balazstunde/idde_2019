$.get("/artists", function(data, status){
	console.log(`${data}`);
	data.forEach((obj, i) => {
		console.log(obj);
		$('#artist tr:last').after('<tr><td>' + obj.name + '</td>' +
				'<td>' + obj.birthday + '</td>' + 
				'<td>' + obj.email + '</td>' +
				'<td> <span class = "glyphicon glyphicon-trash" onclick="deleteArtist(' + obj.id + ')"></span>' + 
				'</td>')
	})
})

function deleteArtist(id){
	console.log(id);

	$.ajax({
	    url: '/artists/'+id,
	    type: 'DELETE',
	    success: function(result) {
	        console.log(result);
	    }
	});
	location.reload();
}

function addArtist(){
	const mydata ={
			name:document.getElementById("name").value,
			birthday:document.getElementById("birthday").value,
			email:document.getElementById("email").value
	}
	
	$.ajax({
		headers: { 
	        'Accept': 'application/json',
	        'Content-Type': 'application/json' 
	    },
	    url: '/artists',
	    type: 'POST',
	    data: JSON.stringify(mydata),
	    success: function(result) {
	        console.log(result);
	    }
	});
	location.reload();
}

function newArtistForm(){
	document.getElementById("newArtistForm").innerHTML= '<form>' + 
    	'<div class="form-group">'+
		    '<label>Name:</label>'+
		    '<input type="text" class="form-control" id="name" name="name">' +
		    '</div>'+
	    '<div class="form-group">'+
		    '<label>Birthday:</label>'+
		    '<input type="date" class="form-control" id="birthday" name="birthday">' +
	    '</div>'+
	    '<div class="form-group">'+
	    	'<label>Email:</label>'+
	    	'<input type="email" class="form-control" id="email"  name="email">' +
	    '</div>'+
	    '</form>' +
	    '<button class="btn btn-default" onclick = "addArtist()">Submit</button>'
}
