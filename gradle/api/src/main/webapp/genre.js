$.get("/genres", function(data, status){
	console.log(`${data}`);
	data.forEach((obj, i) => {
		console.log(obj);
		$('#genre tr:last').after('<tr><td>' + obj.name + '</td>' +
				'<td>' + obj.features + '</td>' + 
				'<td>' + obj.representative + '</td>' +
				'<td> <span class = "glyphicon glyphicon-trash" onclick="deleteGenre(' + obj.id + ')"></span>' + 
				'</td>')
	})
})

function deleteGenre(id){
	console.log(id);

	$.ajax({
	    url: '/genres/'+id,
	    type: 'DELETE',
	    success: function(result) {
	        console.log(result);
	    }
	});
	location.reload();
}

function addGenre(){
	const mydata ={
			name:document.getElementById("name").value,
			features:document.getElementById("features").value,
			representative:document.getElementById("representative").value
	}
	
	$.ajax({
		headers: { 
	        'Accept': 'application/json',
	        'Content-Type': 'application/json' 
	    },
	    url: '/genres',
	    type: 'POST',
	    data: JSON.stringify(mydata),
	    success: function(result) {
	        console.log(result);
	    }
	});
	location.reload();
}

function newGenreForm(){
	document.getElementById("newGenreForm").innerHTML= '<form>' + 
    	'<div class="form-group">'+
		    '<label>Name:</label>'+
		    '<input type="text" class="form-control" id="name" name="name">' +
		    '</div>'+
	    '<div class="form-group">'+
		    '<label>Features:</label>'+
		    '<input type="text" class="form-control" id="features" name="features">' +
	    '</div>'+
	    '<div class="form-group">'+
	    	'<label>Representative:</label>'+
	    	'<input type="text" class="form-control" id="representative"  name="representative">' +
	    '</div>'+
	    '</form>' +
	    '<button class="btn btn-default" onclick = "addGenre()">Submit</button>'
}