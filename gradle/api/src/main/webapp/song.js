$.get("/songs", function(data, status){
	console.log(`${data}`);
	data.forEach((obj, i) => {
		console.log(obj);
		$('#song tr:last').after('<tr><td>' + obj.artist + '</td>' +
				'<td>' + obj.title + '</td>' + 
				'<td>' + obj.album + '</td>' +
				'<td>' + obj.citation + '</td>' +
				'<td>' + obj.year + '</td>' +
				'<td> <span class = "glyphicon glyphicon-trash" onclick="deleteSong(' + obj.id + ')"></span>' + 
				'</td>')
	})
})

function deleteSong(id){
	console.log(id);

	$.ajax({
	    url: '/songs/'+id,
	    type: 'DELETE',
	    success: function(result) {
	        console.log(result);
	    }
	});
	location.reload();
}

function addSong(){
	const mydata ={
			artist:document.getElementById("artist").value,
			title:document.getElementById("title").value,
			album:document.getElementById("album").value,
			citation:document.getElementById("citation").value,
			year:document.getElementById("year").value
	}
	
	$.ajax({
		headers: { 
	        'Accept': 'application/json',
	        'Content-Type': 'application/json' 
	    },
	    url: '/songs',
	    type: 'POST',
	    data: JSON.stringify(mydata),
	    success: function(result) {
	        console.log(result);
	    }
	});
	location.reload();
}

function newSongForm(){
	console.log('Mennie kell');
	document.getElementById("newSongForm").innerHTML= '<form>' + 
    	'<div class="form-group">'+
		    '<label>Artist:</label>'+
		    '<input type="text" class="form-control" id="artist" name="artist">' +
		    '</div>'+
	    '<div class="form-group">'+
		    '<label>Title:</label>'+
		    '<input type="text" class="form-control" id="title" name="title">' +
	    '</div>'+
	    '<div class="form-group">'+
	    	'<label>Album:</label>'+
	    	'<input type="text" class="form-control" id="album"  name="album">' +
	    '</div>'+
	    '<div class="form-group">'+
		    '<label>Citation:</label>'+
		    '<input type="text" class="form-control" id="citation" name="citation">' +
	    '</div>'+
	    '<div class="form-group">'+
		    '<label>Year:</label>'+
		    '<input type="number" class="form-control" id="year"  name="year">' +
	    '</div>'+
	    
	    '</form>' +
	    '<button class="btn btn-default" onclick = "addSong()">Submit</button>'
}

console.log("Hello");