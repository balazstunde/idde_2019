package edu.ubb.cs.idde.beim1596_frontend;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import edu.ubb.cs.idde.beim1596_backend.model.Artist;
import edu.ubb.cs.idde.beim1596_backend.model.Genre;
import edu.ubb.cs.idde.beim1596_backend.model.Song;
import edu.ubb.cs.idde.beim1596_backend.service.ArtistService;
import edu.ubb.cs.idde.beim1596_backend.service.GenreService;
import edu.ubb.cs.idde.beim1596_backend.service.ServiceException;
import edu.ubb.cs.idde.beim1596_backend.service.SongService;


public class SwingComponent 
{
    public static void main( String[] args )
    {
    	JFrame frame = new JFrame();

        frame.setLayout(new BorderLayout());
        //Locale.setDefault(new Locale("en", "US"));
        System.out.println("Current Locale: " + Locale.getDefault());
        ResourceBundle mybundle = ResourceBundle.getBundle("MyGenres");
        //ResourceBundle mybundle = ResourceBundle.getBundle("MyArtists");
        //ResourceBundle mybundle = ResourceBundle.getBundle("MyLabels");
        System.out.println("Say name in Hungarian: " + mybundle.getString("name"));
        
        //reflection for the header names
		Class<?> concreteClass;
		Vector<String> col1 = new Vector<String>();
        try {
        	concreteClass = Class.forName("edu.ubb.cs.idde.beim1596_backend.model.Genre");
        	//concreteClass = Class.forName("edu.ubb.cs.idde.beim1596_backend.model.Artist");
           	//concreteClass = Class.forName("edu.ubb.cs.idde.beim1596_backend.model.Song");
            Field fields[] = concreteClass.getDeclaredFields();
            for (int i = 0; i < fields.length;i++) {  
                 Field f = fields[i];
                 col1.add(mybundle.getString(f.getName()));
                 System.out.println("name = " + f.getName());
            }
         }
	     catch (Throwable ex) {
	        System.err.println(ex);
	     }
        
        final DefaultTableModel tableModel = new DefaultTableModel(col1, 0);
                                                    // The 0 argument is number rows.

        JTable table = new JTable(tableModel);

        JPanel btnPnl = new JPanel(new BorderLayout());
        JButton button = new JButton("GET");
        
        button.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				tableModel.setRowCount(0);
				GenreService genreService = new GenreService();
				List<Genre> genres = null;
				//ArtistService artistService = new ArtistService();
				//List<Artist> artists = null;
				//SongService songService = new SongService();
				//List<Song> songs = null;
				
				try {
					genres = genreService.findAll();
					//artists = artistService.findAll();
					//songs = songService.findAll();
				}catch(ServiceException ex) {
					System.out.println(ex.getMessage());
				}
				
				for(Genre temp : genres) {
				//for(Artist temp : artists) {
				//for (Song temp : songs) {
					Vector<Object> obj1 = new Vector<Object>();
					
					Class<?> concreteClass;
					
					try {
						concreteClass = Class.forName("edu.ubb.cs.idde.beim1596_backend.model.Genre");
						//concreteClass = Class.forName("edu.ubb.cs.idde.beim1596_backend.model.Artist");
						//concreteClass = Class.forName("edu.ubb.cs.idde.beim1596_backend.model.Song");
			            
			           	//refection for field because the order
			           	Field fields[] = concreteClass.getDeclaredFields();
			            for (int i = 0; i < fields.length;i++) {  
			                 Field f = fields[i];
			                 String fieldName = f.getName();
			                 //System.out.println("name = " + fieldName);
			                 //build the method name from fieldName
			                 String methodName = "get"+fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
			                 //System.out.println(methodName);
			                 Method method = concreteClass.getDeclaredMethod(methodName);
				             obj1.add(method.invoke(temp));
			            }
			           	
			         }
				     catch (Throwable ex) {
				        System.err.println(ex);
				     }
					
					tableModel.addRow(obj1);
					System.out.format("%d, %s, %s, %s\n", temp.getId(), temp.getName(), temp.getFeatures(), temp.getRepresentative());
					//System.out.format("%d, %s, %s, %s\n", temp.getId(), temp.getName(), temp.getEmail(), temp.getBirthday());
					//System.out.format("%d, %s, %s, %s, %d, %s\n", temp.getId(), temp.getArtist(), temp.getTitle(), temp.getAlbum(), temp.getYear(), temp.getCitation());
				}
			}
		});

        btnPnl.add(button, BorderLayout.CENTER);

        JScrollPane scrollPane = new JScrollPane(table);
        frame.add(scrollPane, BorderLayout.CENTER);
        frame.add(btnPnl, BorderLayout.SOUTH);

        frame.setTitle("Songs");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
   
    }
}
