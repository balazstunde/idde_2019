package edu.ubb.cs.idde.beim1596_backend.repository.JDBC;

public class JDBCConfiguration {
	public static final String connectionString = "jdbc:mysql://127.0.0.1:3306/tervezoi?serverTimezone=UTC&useSSL=false";
	public static final String user = "root";
	public static final String password = "admin123";
}
