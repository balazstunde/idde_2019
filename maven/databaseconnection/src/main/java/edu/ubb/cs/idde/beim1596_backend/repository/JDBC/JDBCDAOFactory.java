package edu.ubb.cs.idde.beim1596_backend.repository.JDBC;

import edu.ubb.cs.idde.beim1596_backend.model.Artist;
import edu.ubb.cs.idde.beim1596_backend.model.Genre;
import edu.ubb.cs.idde.beim1596_backend.model.Song;
import edu.ubb.cs.idde.beim1596_backend.repository.GenericDAO;
import edu.ubb.cs.idde.beim1596_backend.repository.DAOFactory;

public class JDBCDAOFactory extends DAOFactory {

	@Override
	public GenericDAO<Song> getSongDAO() {
		return new JDBCSongDAO();
	}

	@Override
	public GenericDAO<Genre> getGenreDAO() {
		return new JDBCGenreDAO();
	}

	@Override
	public GenericDAO<Artist> getArtistDAO() {
		return new JDBCArtistDAO();
	}

}
