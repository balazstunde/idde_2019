package edu.ubb.cs.idde.beim1596_backend;

import java.util.List;

import edu.ubb.cs.idde.beim1596_backend.model.Song;
import edu.ubb.cs.idde.beim1596_backend.service.ServiceException;
import edu.ubb.cs.idde.beim1596_backend.service.SongService;

public class Application {
	
	public static void main(String[] args) {
		
		SongService songService = new SongService();
		
		List<Song> songs = null; 
		try {
			songs = songService.findAll();
		}catch(ServiceException ex) {
			System.out.println(ex.getMessage());
		}
		System.out.println("Songs :");
		
		for (Song temp : songs) {
			System.out.format("%d, %s, %s, %s, %d, %s\n", temp.getId(), temp.getArtist(), temp.getTitle(), temp.getAlbum(), temp.getYear(), temp.getCitation());
		}
		
		Song song = new Song(15, "artist", "title", "album", "citation", 1997);
		System.out.println("*** CREATE - start ***");
		try{
			songService.create(song);
		}catch(ServiceException ex) {
			System.out.println(ex.getMessage());
		}
		
		try{
			songs = songService.findAll();
		}catch(ServiceException ex) {
			System.out.println(ex.getMessage());
		}
		for (Song temp : songs) {
			System.out.format("%d, %s, %s, %s, %d, %s\n", temp.getId(), temp.getArtist(), temp.getTitle(), temp.getAlbum(), temp.getYear(), temp.getCitation());
		}
		
		Song song1 = new Song(12, "artistasd", "titleasd", "album", "citation", 1997);
		System.out.println("*** UPDATE - start ***");
		
		try{
			songService.update(song1);
		}catch(ServiceException ex) {
			System.out.println(ex.getMessage());
		}
		try{
			songs = songService.findAll();
		}catch(ServiceException ex) {
			System.out.println(ex.getMessage());
		}
		for (Song temp : songs) {
			System.out.format("%d, %s, %s, %s, %d, %s\n", temp.getId(), temp.getArtist(), temp.getTitle(), temp.getAlbum(), temp.getYear(), temp.getCitation());
		}
		
		
		Song song2 = new Song(10, "artist", "title", "album", "citation", 1997);
		System.out.println("*** DELETE - start ***");
		try{
			songService.delete(song2);
			songs = songService.findAll();
		}catch(ServiceException ex) {
			System.out.println(ex.getMessage());
		}
		
		
		for (Song temp : songs) {
			System.out.format("%d, %s, %s, %s, %d, %s\n", temp.getId(), temp.getArtist(), temp.getTitle(), temp.getAlbum(), temp.getYear(), temp.getCitation());
		}

	}
}
