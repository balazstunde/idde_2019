package edu.ubb.cs.idde.beim1596_backend.repository.JDBC;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import edu.ubb.cs.idde.beim1596_backend.model.Artist;
import edu.ubb.cs.idde.beim1596_backend.repository.GenericDAO;
import edu.ubb.cs.idde.beim1596_backend.repository.RepositoryException;

public class JDBCArtistDAO implements GenericDAO<Artist> {

	JDBCConnectionManager cm = new JDBCConnectionManager();
	private Connection conn = cm.getConnection();
	private PreparedStatement preparedStatement = null;
	
	public List<Artist> findAll() {
		List<Artist> artists = new ArrayList<Artist>();
		try {
			String getQueryStatement = "SELECT * FROM artists";
			preparedStatement = conn.prepareStatement(getQueryStatement);
			
			ResultSet resultSet = preparedStatement.executeQuery();
			
			while(resultSet.next()) {
				Integer id = resultSet.getInt(1);
				String name = resultSet.getString(2);
				Date birthday = resultSet.getDate(3);
				String email = resultSet.getString(4);
				artists.add(new Artist(id, name, birthday, email));
				//System.out.format("%d, %s, %s, %s, %d, %s\n", id, artist, title, album, year, quote);
			}
		}catch(SQLException e) {
			throw (new RepositoryException("Prepared statement failed.", e));
		}/*finally {
			try {
				cm.closeJDBCConnection();
			}catch(RepositoryException e) {
				throw (new RepositoryException("Close failed.", e));
			}
		}*/
		return artists;
	}

	public void update(Artist artist) {
		try {
			String insertQueryStatement = "UPDATE artists  SET id = ?, artist_name = ?, birthday = ?, email = ? WHERE  id = ?";
			
			preparedStatement = conn.prepareStatement(insertQueryStatement);
			preparedStatement.setInt(1, artist.getId());
			preparedStatement.setString(2, artist.getName());
			preparedStatement.setDate(3, (java.sql.Date) artist.getBirthday());
			preparedStatement.setString(4, artist.getEmail());
			preparedStatement.setInt(5, artist.getId());
 
			// execute insert SQL statement
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new RepositoryException("Update failed.", e);
		}
		
	}

	public void create(Artist artist) {
		try {
			String insertQueryStatement = "INSERT  INTO  artists  VALUES  (?,?,?,?)";
			
			preparedStatement = conn.prepareStatement(insertQueryStatement);
			preparedStatement.setInt(1, artist.getId());
			preparedStatement.setString(2, artist.getName());
			preparedStatement.setDate(3, (java.sql.Date) artist.getBirthday());
			preparedStatement.setString(4, artist.getEmail());
 
			// execute insert SQL statement
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new RepositoryException("Create failed.", e);
		}
		
	}

	public void delete(Artist artist) {
		try {
			String insertQueryStatement = "DELETE FROM artists WHERE id=?";
			
			preparedStatement = conn.prepareStatement(insertQueryStatement);
			preparedStatement.setInt(1, artist.getId());
 
			// execute insert SQL statement
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new RepositoryException("Delete failed.", e);
		}
		
	}

}
