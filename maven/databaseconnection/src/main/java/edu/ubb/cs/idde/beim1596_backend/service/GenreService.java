package edu.ubb.cs.idde.beim1596_backend.service;

import java.util.List;

import edu.ubb.cs.idde.beim1596_backend.model.Genre;
import edu.ubb.cs.idde.beim1596_backend.repository.DAOFactory;
import edu.ubb.cs.idde.beim1596_backend.repository.GenericDAO;
import edu.ubb.cs.idde.beim1596_backend.repository.RepositoryException;

public class GenreService {
	private DAOFactory myFactory = DAOFactory.getDAOFactory(DAOFactory.JDBC);
	private GenericDAO<Genre> genreDAO = myFactory.getGenreDAO();
	
	public List<Genre> findAll(){
		List<Genre> genres = null;
		try{
			genres = genreDAO.findAll();
		}catch(RepositoryException e) {
			throw new ServiceException("Exception in service layer.", e);
		}
		return genres;
	}
	
	public void create(Genre genre) {
		try {
			genreDAO.create(genre);
		}catch(RepositoryException e) {
			throw new ServiceException("Exception in service layer.", e);
		}
	}
	
	public void update(Genre genre) {
		try {
			genreDAO.update(genre);
		}catch(RepositoryException e) {
			throw new ServiceException("Exception in service layer.", e);
		}
	}
	
	public void delete(Genre genre) {
		try {
			genreDAO.delete(genre);
		}catch(RepositoryException e) {
			throw new ServiceException("Exception in service layer.", e);
		}
	}
}
