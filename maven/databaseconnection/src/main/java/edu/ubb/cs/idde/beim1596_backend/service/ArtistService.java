package edu.ubb.cs.idde.beim1596_backend.service;

import java.util.List;

import edu.ubb.cs.idde.beim1596_backend.model.Artist;
import edu.ubb.cs.idde.beim1596_backend.repository.DAOFactory;
import edu.ubb.cs.idde.beim1596_backend.repository.GenericDAO;
import edu.ubb.cs.idde.beim1596_backend.repository.RepositoryException;

public class ArtistService {

	private DAOFactory myFactory = DAOFactory.getDAOFactory(DAOFactory.JDBC);
	private GenericDAO<Artist> artistDAO = myFactory.getArtistDAO();
	
	public List<Artist> findAll(){
		List<Artist> artists = null;
		try{
			artists = artistDAO.findAll();
		}catch(RepositoryException e) {
			throw new ServiceException("Exception in service layer.", e);
		}
		return artists;
	}
	
	public void create(Artist artist) {
		try {
			artistDAO.create(artist);
		}catch(RepositoryException e) {
			throw new ServiceException("Exception in service layer.", e);
		}
	}
	
	public void update(Artist artist) {
		try {
			artistDAO.update(artist);
		}catch(RepositoryException e) {
			throw new ServiceException("Exception in service layer.", e);
		}
	}
	
	public void delete(Artist artist) {
		try {
			artistDAO.delete(artist);
		}catch(RepositoryException e) {
			throw new ServiceException("Exception in service layer.", e);
		}
	}
}
