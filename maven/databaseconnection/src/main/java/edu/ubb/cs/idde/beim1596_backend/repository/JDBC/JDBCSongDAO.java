package edu.ubb.cs.idde.beim1596_backend.repository.JDBC;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.ubb.cs.idde.beim1596_backend.model.Song;
import edu.ubb.cs.idde.beim1596_backend.repository.GenericDAO;
import edu.ubb.cs.idde.beim1596_backend.repository.RepositoryException;

public class JDBCSongDAO implements GenericDAO<Song> {

	final static Logger logger = LoggerFactory.getLogger(JDBCSongDAO.class);
	
	JDBCConnectionManager cm = new JDBCConnectionManager();
	private Connection conn = cm.getConnection();
	private PreparedStatement preparedStatement = null;
	
	public List<Song> findAll() {
		logger.debug("JDBC Song Findall started.");
		logger.info("JDBC Song Findall started.");
		List<Song> songs = new ArrayList<Song>();
		try {
			String getQueryStatement = "SELECT * FROM songs";
			preparedStatement = conn.prepareStatement(getQueryStatement);
			
			ResultSet resultSet = preparedStatement.executeQuery();
			
			while(resultSet.next()) {
				Integer id = resultSet.getInt(1);
				String artist = resultSet.getString(2);
				String title = resultSet.getString(3);
				String album = resultSet.getString(4);
				Integer year = resultSet.getInt(5);
				String quote = resultSet.getString(6);
				songs.add(new Song(id, artist, title, album, quote, year));
				//System.out.format("%d, %s, %s, %s, %d, %s\n", id, artist, title, album, year, quote);
			}
		}catch(SQLException e) {
			throw (new RepositoryException("Prepared statement failed.", e));
		}
		return songs;
	}

	public void update(Song song) {
		logger.debug("JDBC Song Update started.");
		logger.info("JDBC Song Update started.");
		try {
			String insertQueryStatement = "UPDATE songs  SET id = ?, artist = ?, title = ?, album = ?, song_year=?, citation = ?  WHERE  id = ?";
			
			preparedStatement = conn.prepareStatement(insertQueryStatement);
			preparedStatement.setInt(1, song.getId());
			preparedStatement.setString(2, song.getArtist());
			preparedStatement.setString(3, song.getTitle());
			preparedStatement.setString(4, song.getAlbum());
			preparedStatement.setInt(5, song.getYear());
			preparedStatement.setString(6, song.getCitation());
			preparedStatement.setInt(7, song.getId());
 
			// execute insert SQL statement
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new RepositoryException("Update failed.", e);
		}
		
	}

	public void create(Song song) {
		logger.debug("JDBC Song Create started.");
		logger.info("JDBC Song Create started.");
		try {
			String insertQueryStatement = "INSERT  INTO  songs  VALUES  (?,?,?,?,?,?)";
			
			preparedStatement = conn.prepareStatement(insertQueryStatement);
			preparedStatement.setInt(1, song.getId());
			preparedStatement.setString(2, song.getArtist());
			preparedStatement.setString(3, song.getTitle());
			preparedStatement.setString(4, song.getAlbum());
			preparedStatement.setInt(5, song.getYear());
			preparedStatement.setString(6, song.getCitation());
 
			// execute insert SQL statement
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new RepositoryException("Create failed.", e);
		}
		
	}

	public void delete(Song song) {
		logger.debug("JDBC Song Delete started.");
		logger.info("JDBC Song Delete started.");
		try {
			String insertQueryStatement = "DELETE FROM songs WHERE id=?";
			
			preparedStatement = conn.prepareStatement(insertQueryStatement);
			preparedStatement.setInt(1, song.getId());
 
			// execute insert SQL statement
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new RepositoryException("Delete failed.", e);
		}
		
	}

}
