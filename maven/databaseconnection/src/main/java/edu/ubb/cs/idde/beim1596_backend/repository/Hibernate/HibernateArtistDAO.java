package edu.ubb.cs.idde.beim1596_backend.repository.Hibernate;

import java.util.List;

//import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

import edu.ubb.cs.idde.beim1596_backend.model.Artist;
import edu.ubb.cs.idde.beim1596_backend.repository.GenericDAO;
import edu.ubb.cs.idde.beim1596_backend.repository.RepositoryException;


public class HibernateArtistDAO implements GenericDAO<Artist> {

	//final static Logger logger = Logger.getLogger(HibernateArtistDAO.class);
	
	private HibernateConnectionManager cm = new HibernateConnectionManager();

	@SuppressWarnings("unchecked")
	public List<Artist> findAll() {
		List<Artist> artists = null;
		try{
			cm.openCurrentSessionwithTransaction();
			artists = (List<Artist>) cm.getCurrentSession().createQuery("from Artist").list();
		}catch(HibernateException ex) {
			throw new RepositoryException("FindAll failed.", ex);
		}finally {
			cm.closeCurrentSessionwithTransaction();
		}
		
		return artists;
	}

	public void update(Artist artist) {
		try{
			cm.openCurrentSessionwithTransaction();
			cm.getCurrentSession().update(artist);
		}catch(HibernateException ex) {
			throw new RepositoryException("update failed.", ex);
		}finally {
			cm.closeCurrentSessionwithTransaction();
		}
	}

	public void create(Artist artist) {
		try {
			cm.openCurrentSessionwithTransaction();
			cm.getCurrentSession().save(artist);
		}catch(HibernateException ex) {
			throw new RepositoryException("create failed.", ex);
		}finally {
			cm.closeCurrentSessionwithTransaction();
		}
	}

	public void delete(Artist artist) {
		try {
			cm.openCurrentSessionwithTransaction();
			cm.getCurrentSession().delete(artist);
		}catch(HibernateException ex) {
			throw new RepositoryException("delete failed.", ex);
		}finally {
			cm.closeCurrentSessionwithTransaction();
		}
	}

}
