package edu.ubb.cs.idde.beim1596_backend.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "genres")
public class Genre implements Serializable{

	@Id
	private Integer id;
	@Column(name = "genres_name")
	private String name;
	@Column(name = "features")
	private String features;
	@Column(name = "representative")
	private String representative;
	
	public Genre() {
		
	}
	
	public Genre(Integer id, String name, String features, String representative) {
		super();
		this.id = id;
		this.name = name;
		this.features = features;
		this.representative = representative;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFeatures() {
		return features;
	}

	public void setFeatures(String features) {
		this.features = features;
	}

	public String getRepresentative() {
		return representative;
	}

	public void setRepresentative(String representative) {
		this.representative = representative;
	}
	
}
