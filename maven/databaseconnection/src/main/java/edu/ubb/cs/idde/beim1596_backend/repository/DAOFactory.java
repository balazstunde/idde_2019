package edu.ubb.cs.idde.beim1596_backend.repository;

import edu.ubb.cs.idde.beim1596_backend.model.Artist;
import edu.ubb.cs.idde.beim1596_backend.model.Genre;
import edu.ubb.cs.idde.beim1596_backend.model.Song;
import edu.ubb.cs.idde.beim1596_backend.repository.Hibernate.HibernateDAOFactory;
import edu.ubb.cs.idde.beim1596_backend.repository.JDBC.JDBCDAOFactory;

public abstract class DAOFactory {
	
	 public static final int JDBC = 1;
	 public static final int Hibernate = 2;
	 
	 // List of DAO types supported by the factory
	 public abstract GenericDAO<Song> getSongDAO();
	 public abstract GenericDAO<Genre> getGenreDAO();
	 public abstract GenericDAO<Artist> getArtistDAO();
	 

	 // There will be a method for each DAO that can be 
	 // created. The concrete factories will have to 
	 // implement these methods.
	 public static DAOFactory getDAOFactory(int whichFactory) {
		  
		    switch (whichFactory) {
		      case JDBC: 
		          return new JDBCDAOFactory();
		      case Hibernate:
		    	  return new HibernateDAOFactory();
		      default: 
		          return null;
		    }
	 }
}