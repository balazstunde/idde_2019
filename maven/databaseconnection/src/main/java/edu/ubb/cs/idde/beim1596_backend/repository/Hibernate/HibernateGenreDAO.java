package edu.ubb.cs.idde.beim1596_backend.repository.Hibernate;

import java.util.List;

import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.ubb.cs.idde.beim1596_backend.model.Genre;
import edu.ubb.cs.idde.beim1596_backend.repository.GenericDAO;
import edu.ubb.cs.idde.beim1596_backend.repository.RepositoryException;
import edu.ubb.cs.idde.beim1596_backend.repository.JDBC.JDBCSongDAO;

public class HibernateGenreDAO implements GenericDAO<Genre> {

	private HibernateConnectionManager cm = new HibernateConnectionManager();
	
	final static Logger logger = LoggerFactory.getLogger(JDBCSongDAO.class);
	
	@SuppressWarnings("unchecked")
	public List<Genre> findAll() {
		logger.info("Entered Hibernate in Genre findAll.");
		logger.debug("Entered Hibernate in Genre findAll.");
		List<Genre> genres = null;
		try{
			cm.openCurrentSessionwithTransaction();
			genres = (List<Genre>) cm.getCurrentSession().createQuery("from Genre").list();
		}catch(HibernateException ex) {
			throw new RepositoryException("FindAll failed.", ex);
		}finally{
			cm.closeCurrentSessionwithTransaction();
		}
		
		return genres;
	}

	public void update(Genre genres) {
		logger.info("Entered in Genre update.");
		logger.debug("Entered in Genre update.");
		try{
			cm.openCurrentSessionwithTransaction();
			cm.getCurrentSession().update(genres);
		}catch(HibernateException ex) {
			throw new RepositoryException("update failed.", ex);
		}finally{
			cm.closeCurrentSessionwithTransaction();
		}
	}

	public void create(Genre genres) {
		logger.info("Entered in Genre delete.");
		logger.debug("Entered in Genre delete.");
		try {
			cm.openCurrentSessionwithTransaction();
			cm.getCurrentSession().save(genres);
		}catch(HibernateException ex) {
			throw new RepositoryException("create failed.", ex);
		}finally{
			cm.closeCurrentSessionwithTransaction();
		}
	}

	public void delete(Genre genres) {
		logger.info("Entered in Genre delete.");
		logger.debug("Entered in Genre delete.");
		try {
			cm.openCurrentSessionwithTransaction();
			cm.getCurrentSession().delete(genres);
		}catch(HibernateException ex) {
			throw new RepositoryException("delete failed.", ex);
		}finally{
			cm.closeCurrentSessionwithTransaction();
		}
	}
}
