package edu.ubb.cs.idde.beim1596_backend.repository.JDBC;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import edu.ubb.cs.idde.beim1596_backend.repository.RepositoryException;

public class JDBCConnectionManager {
	
	private Connection connection = null;
	
	public JDBCConnectionManager() {
		makeJDBCConnection();
	}
	
	public Connection getConnection() {
		return connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}
	
	public void makeJDBCConnection() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			System.out.println("Seems your JDBC Driver Registred.");
		}catch(ClassNotFoundException e) {
			System.out.println("Coludn't found JDBC driver.");
			throw (new RepositoryException("Couldn't found JDBC driver.", e));
		}
		
		try {
			connection = DriverManager.getConnection(JDBCConfiguration.connectionString, JDBCConfiguration.user, JDBCConfiguration.password);
			if(connection != null) {
				System.out.println("Connection successful!");
			}else {
				System.out.println("Failed to make connection!");
			}
		}catch(SQLException e) {
			System.out.println("MySQL connection failed.");
			throw (new RepositoryException("MySQL connection failed.", e));
		}
	}
	
	public void closeJDBCConnection() {
		try {
			connection.close();
		} catch (SQLException e) {
			throw new RepositoryException("JDBC close failed.", e);
		}
	}
}
