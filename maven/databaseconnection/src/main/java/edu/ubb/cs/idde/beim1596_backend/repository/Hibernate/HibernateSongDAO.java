package edu.ubb.cs.idde.beim1596_backend.repository.Hibernate;

import java.util.List;

import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.ubb.cs.idde.beim1596_backend.model.Song;
import edu.ubb.cs.idde.beim1596_backend.repository.GenericDAO;
import edu.ubb.cs.idde.beim1596_backend.repository.RepositoryException;
import edu.ubb.cs.idde.beim1596_backend.repository.JDBC.JDBCSongDAO;

public class HibernateSongDAO implements GenericDAO<Song>{

	final static Logger logger = LoggerFactory.getLogger(JDBCSongDAO.class);
	
	private HibernateConnectionManager cm = new HibernateConnectionManager();

		@SuppressWarnings("unchecked")
	public List<Song> findAll() {
		logger.info("Entered Hibernate in Song findAll.");
		logger.debug("Entered Hibernate in Song findAll.");
		List<Song> songs = null;
		try{
			cm.openCurrentSessionwithTransaction();
			songs = (List<Song>) cm.getCurrentSession().createQuery("from Song").list();
		}catch(HibernateException ex) {
			throw new RepositoryException("FindAll failed.", ex);
		}finally {
			cm.closeCurrentSessionwithTransaction();
		}
		return songs;
	}

	public void update(Song song) {
		logger.info("Entered Hibernate in Song update.");
		logger.debug("Entered Hibernate in Song update.");
		try{
			cm.openCurrentSessionwithTransaction();
			cm.getCurrentSession().update(song);
		}catch(HibernateException ex) {
			throw new RepositoryException("update failed.", ex);
		}finally {
			cm.closeCurrentSessionwithTransaction();
		}
	}

	public void create(Song song) {
		logger.info("Entered Hibernate in Song create.");
		logger.debug("Entered Hibernate in Song create.");
		try {
			cm.openCurrentSessionwithTransaction();
			cm.getCurrentSession().save(song);
		}catch(HibernateException ex) {
			throw new RepositoryException("create failed.", ex);
		}finally {
			cm.closeCurrentSessionwithTransaction();
		}
	}

	public void delete(Song song) {
		logger.info("Entered Hibernate in Song delete.");
		logger.debug("Entered Hibernate in Song delete.");
		try {
			cm.openCurrentSessionwithTransaction();
			cm.getCurrentSession().delete(song);
		}catch(HibernateException ex) {
			throw new RepositoryException("delete failed.", ex);
		}finally {
			cm.closeCurrentSessionwithTransaction();
		}
	}
}
