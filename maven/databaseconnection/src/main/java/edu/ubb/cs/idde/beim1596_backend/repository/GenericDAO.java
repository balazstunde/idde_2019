package edu.ubb.cs.idde.beim1596_backend.repository;

import java.util.List;

public interface GenericDAO<P> {
	List<P> findAll();
	void update(P p);
	void create(P p);
	void delete(P p);
}
