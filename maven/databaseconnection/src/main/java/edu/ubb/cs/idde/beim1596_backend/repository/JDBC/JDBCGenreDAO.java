package edu.ubb.cs.idde.beim1596_backend.repository.JDBC;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.ubb.cs.idde.beim1596_backend.model.Genre;
import edu.ubb.cs.idde.beim1596_backend.repository.GenericDAO;
import edu.ubb.cs.idde.beim1596_backend.repository.RepositoryException;

public class JDBCGenreDAO implements GenericDAO<Genre> {

	final static Logger logger = LoggerFactory.getLogger(JDBCSongDAO.class);
	
	JDBCConnectionManager cm = new JDBCConnectionManager();
	private Connection conn = cm.getConnection();
	private PreparedStatement preparedStatement = null;
	
	public List<Genre> findAll() {
		logger.info("Entered in JDBC Genre findAll.");
		logger.debug("Entered in JDBC Genre findAll.");
		List<Genre> genres = new ArrayList<Genre>();
		try {
			String getQueryStatement = "SELECT * FROM genres";
			preparedStatement = conn.prepareStatement(getQueryStatement);
			
			ResultSet resultSet = preparedStatement.executeQuery();
			
			while(resultSet.next()) {
				Integer id = resultSet.getInt(1);
				String name = resultSet.getString(2);
				String features = resultSet.getString(3);
				String representative = resultSet.getString(4);
				genres.add(new Genre(id, name, features, representative));
				//System.out.format("%d, %s, %s, %s, %d, %s\n", id, artist, title, album, year, quote);
			}
		}catch(SQLException e) {
			throw (new RepositoryException("Prepared statement failed.", e));
		}/*finally {
			try {
				cm.closeJDBCConnection();
			}catch(RepositoryException e) {
				throw (new RepositoryException("Close failed.", e));
			}
		}*/
		return genres;
	}

	public void update(Genre genre) {
		logger.info("Entered in JDBC Genre update.");
		logger.debug("Entered in JDBC Genre update.");
		try {
			String insertQueryStatement = "UPDATE genres  SET id = ?, genres_name = ?, features = ?, representative = ? WHERE  id = ?";
			
			preparedStatement = conn.prepareStatement(insertQueryStatement);
			preparedStatement.setInt(1, genre.getId());
			preparedStatement.setString(2, genre.getName());
			preparedStatement.setString(3, genre.getFeatures());
			preparedStatement.setString(4, genre.getRepresentative());
			preparedStatement.setInt(5, genre.getId());
 
			// execute insert SQL statement
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new RepositoryException("Update failed.", e);
		}
		
	}

	public void create(Genre genre) {
		logger.info("Entered in JDBC Genre create.");
		logger.debug("Entered in JDBC Genre create.");
		try {
			String insertQueryStatement = "INSERT  INTO  genres  VALUES  (?,?,?,?)";
			
			preparedStatement = conn.prepareStatement(insertQueryStatement);
			preparedStatement.setInt(1, genre.getId());
			preparedStatement.setString(2, genre.getName());
			preparedStatement.setString(3, genre.getFeatures());
			preparedStatement.setString(4, genre.getRepresentative());
 
			// execute insert SQL statement
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new RepositoryException("Create failed.", e);
		}
		
	}

	public void delete(Genre genre) {
		logger.info("Entered in JDBC Genre delete.");
		logger.debug("Entered in JDBC Genre delete.");
		try {
			String insertQueryStatement = "DELETE FROM genres WHERE id=?";
			
			preparedStatement = conn.prepareStatement(insertQueryStatement);
			preparedStatement.setInt(1, genre.getId());
 
			// execute insert SQL statement
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new RepositoryException("Delete failed.", e);
		}
		
	}

}
