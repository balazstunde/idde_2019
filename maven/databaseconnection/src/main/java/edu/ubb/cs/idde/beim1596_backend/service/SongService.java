package edu.ubb.cs.idde.beim1596_backend.service;

import java.util.List;

import edu.ubb.cs.idde.beim1596_backend.model.Song;
import edu.ubb.cs.idde.beim1596_backend.repository.DAOFactory;
import edu.ubb.cs.idde.beim1596_backend.repository.GenericDAO;
import edu.ubb.cs.idde.beim1596_backend.repository.RepositoryException;

public class SongService {
	
	private DAOFactory myFactory = DAOFactory.getDAOFactory(DAOFactory.Hibernate);
	private GenericDAO<Song> songDAO = myFactory.getSongDAO();
	
	public List<Song> findAll(){
		List<Song> songs = null;
		try{
			songs = songDAO.findAll();
		}catch(RepositoryException e) {
			throw new ServiceException("Exception in service layer.", e);
		}
		return songs;
	}
	
	public void create(Song song) {
		try {
			songDAO.create(song);
		}catch(RepositoryException e) {
			throw new ServiceException("Exception in service layer.", e);
		}
	}
	
	public void update(Song song) {
		try {
			songDAO.update(song);
		}catch(RepositoryException e) {
			throw new ServiceException("Exception in service layer.", e);
		}
	}
	
	public void delete(Song song) {
		try {
			songDAO.delete(song);
		}catch(RepositoryException e) {
			throw new ServiceException("Exception in service layer.", e);
		}
	}
}
